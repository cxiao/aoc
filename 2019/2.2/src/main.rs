use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::vec::Vec;

fn parse_raw(input: &str) -> Vec<u32> {
    input
        .trim()
        .split(',')
        .map(|n| {
            n.parse::<u32>()
                .expect(&format!("Could not parse value {}", n))
        })
        .collect()
}

fn compute(data: &mut Vec<u32>) {
    for i in (0..data.len()).step_by(4) {
        // Ensure there's enough instructions left
        if data.len() - i >= 4 {
            let opcode: u32 = data[i as usize];
            let x_idx: usize = data[i as usize + 1] as usize;
            let y_idx: usize = data[i as usize + 2] as usize;
            let result_idx: usize = data[i as usize + 3] as usize;
            match opcode {
                1 => { data[result_idx] = data[x_idx] + data[y_idx]; },
                2 => { data[result_idx] = data[x_idx] * data[y_idx]; },
                99 => break,
                _ => panic!("Error: Encountered unknown instruction {} at index {} of input data", opcode, i),
            }
        }
    }
}

fn take_inputs(noun: u32, verb: u32, memory: &mut Vec<u32>) {
    memory[1] = noun;
    memory[2] = verb;
}

#[cfg(test)]
mod tests {
    use crate::compute;

    #[test]
    fn test_add() {
        let input = vec![1,0,0,0,99];
        let mut output = input.clone();
        let output_good = vec![2,0,0,0,99];
        compute(&mut output);
        assert_eq!(output_good, output);
    }

    #[test]
    fn test_mult_1() {
        let input = vec![2,3,0,3,99];
        let mut output = input.clone();
        let output_good = vec![2,3,0,6,99];
        compute(&mut output);
        assert_eq!(output_good, output);
    }

    #[test]
    fn test_mult_2() {
        let input = vec![2,4,4,5,99,0];
        let mut output = input.clone();
        let output_good = vec![2,4,4,5,99,9801];
        compute(&mut output);
        assert_eq!(output_good, output);
    }

    #[test]
    fn test_all_ops() {
        let input = vec![1,1,1,4,99,5,6,0,99];
        let mut output = input.clone();
        let output_good = vec![30,1,1,4,2,5,6,0,99];
        compute(&mut output);
        assert_eq!(output_good, output);
    }

}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 3 {
        if let Ok(goal_output) = &args[2].parse::<u32>() {
            if let Ok(mut infile) = File::open(&args[1]) {
                let mut raw_input = String::new();
                infile.read_to_string(&mut raw_input).unwrap();
                let program = parse_raw(&raw_input);

                // Brute force through possible program inputs
                for noun in 0..99 {
                    for verb in 0..99 {
                        let mut memory = program.clone();
                        take_inputs(noun, verb, &mut memory);
                        compute(&mut memory);
                        if memory[0] == *goal_output {
                            println!("noun: {}, verb: {}, output: {}", noun, verb, goal_output);
                            break;
                        }
                    }
                }
            } else {
                println!("Invalid input filename. Exiting");
            }
        } else {
            println!("Invalid number provided for expected output. Exiting");
        }
    } else {
        println!("Incorrect number of arguments provided. Usage: aoc-2019-2-2 [INPUT_FILE] [EXPECTED_OUTPUT]. Exiting");
    }
}
