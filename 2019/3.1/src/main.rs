use std::collections::BTreeSet;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::vec::Vec;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    /// Return the Manhattan distance from the origin (0, 0).
    fn manhattan_distance(self) -> i64 {
        self.x.abs() as i64 + self.y.abs() as i64
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
struct Segment {
    origin: Point,
    end: Point,
}

#[derive(Debug, PartialEq)]
struct Line {
    horiz: Vec<Segment>,
    vert: Vec<Segment>,
}

impl Line {
    fn new() -> Line {
        let horiz: Vec<Segment> = Vec::new();
        let vert: Vec<Segment> = Vec::new();
        Line {
            horiz: horiz,
            vert: vert,
        }
    }
}

/// Parses the provided input string into a Segment.
///
/// Arguments:
///
/// * `input`: The input string, indicating the direction
/// (indicated with one of the letters 'U', 'D', 'L', or 'R')
/// and the distance of the segment away from the origin point.
/// 'U' means increasing y-coordinates; 'R' means increasing x-coordinates.
/// * `origin`: The Point that the segment should extend from.
///
/// For example, `L34` should be parsed as a segment whose endpoint lies
/// -34 away from the provided origin point, on the x-axis.
///
/// Return a tuple, with the first element being the parsed segment
/// and the second element being whether it is horizontal or vertical
/// (with true being horizontal).
fn parse_segment(input: &str, origin: Point) -> (Segment, bool) {
    let (dir, len) = input.split_at(1);
    let len = len.parse::<i32>().unwrap();
    let mut end: Point = origin;
    let horiz: bool;
    match dir {
        "U" => {
            end.y += len;
            horiz = false;
        }
        "D" => {
            end.y -= len;
            horiz = false;
        }
        "L" => {
            end.x -= len;
            horiz = true;
        }
        "R" => {
            end.x += len;
            horiz = true;
        }
        _ => panic!("Error: Unknown direction {} encountered.", dir),
    }
    (
        Segment {
            origin: origin,
            end: end,
        },
        horiz,
    )
}

/// Parses the provided input string into a Line.
/// Each segment in the input string should be separated by a comma.
/// For example, the following is parsed as a Line with 4 Segments:
/// ```
/// R5,D19,L23,D4
/// ```
fn parse_line(input: &str) -> Line {
    let mut line = Line::new();
    let mut origin = Point { x: 0, y: 0 };
    let raw_segments: Vec<&str> = input.split(',').collect();
    for s in raw_segments {
        let (segment, horiz) = parse_segment(s, origin);
        origin = segment.end;
        if horiz {
            line.horiz.push(segment);
        } else {
            line.vert.push(segment);
        }
    }
    line
}

/// Parses the provided input string into a Vec of Lines.
/// Each Line in the provided input string should be separated by a newline.
/// For example, the following is parsed as 2 Lines:
///
/// ```
/// R5,D19,L23,D4
/// U45,L108
/// ```
fn parse_raw(input: &str) -> Vec<Line> {
    input
        .trim()
        .split('\n')
        .map(|l| parse_line(l.trim()))
        .collect()
}

/// Find all intersections between a list of horizontal segments
/// and vertical segments.
///
/// Returns the intersections, as a set of `Point` structs.
///
/// Arguments:
///
/// * `horiz`: The list of horizontal segments.
/// * `vert`: The list of horizontal segments.
fn find_intersections(horiz: &Vec<Segment>, vert: &Vec<Segment>) -> BTreeSet<Point> {
    // Use a sweep line algorithm.

    // Represent the segments as events that an imaginary vertical line
    // sweeping across the grid would encounter.
    // These include the beginning of a horizontal line segment,
    // the end of a horizontal line segment, and a vertical line.
    #[derive(Debug)]
    enum SweepEvent {
        HorizBegin { point: Point },
        HorizEnd { point: Point },
        Vertical { segment: Segment },
    }

    let mut intersections: BTreeSet<Point> = BTreeSet::new();

    // Construct the sweep event list.
    // Collapse horizontal and vertical segments into the same list.
    let mut sweep_list: Vec<SweepEvent> = Vec::new();

    for segment in horiz {
        sweep_list.push(SweepEvent::HorizBegin {
            point: {
                if segment.origin.x < segment.end.x {
                    segment.origin
                } else {
                    segment.end
                }
            },
        });
        sweep_list.push(SweepEvent::HorizEnd {
            point: {
                if segment.end.x > segment.origin.x {
                    segment.end
                } else {
                    segment.origin
                }
            },
        });
    }

    for segment in vert {
        sweep_list.push(SweepEvent::Vertical { segment: *segment });
    }

    // Sort the list of sweep events by increasing x-coordinate, then increasing y-coordinate.
    sweep_list.sort_by_key(|event| {
        (
            match event {
                SweepEvent::HorizBegin { point } => point.x,
                SweepEvent::HorizEnd { point } => point.x,
                SweepEvent::Vertical { segment } => segment.origin.x,
            },
            match event {
                SweepEvent::HorizBegin { point } => point.y,
                SweepEvent::HorizEnd { point } => point.y,
                SweepEvent::Vertical { segment } => segment.origin.y,
            },
        )
    });

    // Keep a set of active horizontal segments, so that we can look for
    // intersections when we encounter a vertical segment.
    // Since we're sweeping over a sorted list in increasing x-coordinate order,
    // and line segments are removed from the list as soon as they end,
    // we can just keep track of the active horizontal segments by
    // their y-coordinates.
    let mut active_horiz: BTreeSet<i32> = BTreeSet::new();

    // Examine the list of sweep events.
    // If we encounter a horizontal begin or end event,
    // update the set of active horizontal segments.
    // If we encounter a vertical event, do a range search through the set of
    // active horizontal segments to see if the vertical line intersects
    // any of them.
    for event in sweep_list {
        match event {
            SweepEvent::HorizBegin { point } => {
                active_horiz.insert(point.y);
            }
            SweepEvent::HorizEnd { point } => {
                active_horiz.remove(&point.y);
            }
            SweepEvent::Vertical { segment } => {
                for intersection_y in active_horiz.range(if segment.origin.y <= segment.end.y {
                    segment.origin.y..=segment.end.y
                } else {
                    segment.end.y..=segment.origin.y
                }) {
                    intersections.insert(Point {
                        x: segment.origin.x,
                        y: *intersection_y,
                    });
                }
            }
        }
    }

    intersections
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
        if let Ok(mut infile) = File::open(&args[1]) {
            let mut raw_input = String::new();
            infile.read_to_string(&mut raw_input).unwrap();
            let lines = parse_raw(&raw_input);

            // Find the intersections between all of the horizontal segments
            // in line a and all of the vertical segments in line b,
            // and vice versa.
            let intersections_a = find_intersections(&lines[0].horiz, &lines[1].vert);
            let intersections_b = find_intersections(&lines[1].horiz, &lines[0].vert);

            // Get the union of the 2 sets of intersections, and find the one
            // with the closest Manhattan distance.
            let closest_intersection =
                intersections_a
                    .union(&intersections_b)
                    .min_by(|point_a, point_b| {
                        point_a
                            .manhattan_distance()
                            .cmp(&point_b.manhattan_distance())
                    });
            println!(
                "Closest intersection: {:?} at distance {}",
                closest_intersection.unwrap(),
                closest_intersection.unwrap().manhattan_distance()
            );
        } else {
            println!("Invalid input filename. Exiting");
        }
    } else {
        println!(
            "Incorrect number of arguments provided. Usage: aoc-2019-3-1 [INPUT_FILE]. Exiting"
        );
    }
}

#[cfg(test)]
mod tests {
    use crate::{find_intersections, parse_raw, Line, Point, Segment};
    use std::collections::BTreeSet;
    use std::iter::FromIterator;

    #[test]
    fn test_parse_raw() {
        let raw_input = "R5,D19,L23,D4
U45,L108";
        let expected_lines = vec![
            Line {
                horiz: vec![
                    Segment {
                        origin: Point { x: 0, y: 0 },
                        end: Point { x: 5, y: 0 },
                    },
                    Segment {
                        origin: Point { x: 5, y: -19 },
                        end: Point { x: -18, y: -19 },
                    },
                ],
                vert: vec![
                    Segment {
                        origin: Point { x: 5, y: 0 },
                        end: Point { x: 5, y: -19 },
                    },
                    Segment {
                        origin: Point { x: -18, y: -19 },
                        end: Point { x: -18, y: -23 },
                    },
                ],
            },
            Line {
                horiz: vec![Segment {
                    origin: Point { x: 0, y: 45 },
                    end: Point { x: -108, y: 45 },
                }],
                vert: vec![Segment {
                    origin: Point { x: 0, y: 0 },
                    end: Point { x: 0, y: 45 },
                }],
            },
        ];
        let lines = parse_raw(&raw_input);
        assert!(expected_lines == lines);
    }

    #[test]
    fn test_intersections() {
        let horiz_segments = vec![
            Segment {
                origin: Point { x: 0, y: 0 },
                end: Point { x: 8, y: 0 },
            },
            Segment {
                origin: Point { x: 8, y: 5 },
                end: Point { x: 3, y: 5 },
            },
            Segment {
                origin: Point { x: 10, y: 3 },
                end: Point { x: 6, y: 3 },
            },
        ];
        let vert_segments = vec![
            Segment {
                origin: Point { x: 4, y: -1 },
                end: Point { x: 4, y: 2 },
            },
            Segment {
                origin: Point { x: 6, y: 7 },
                end: Point { x: 6, y: 3 },
            },
        ];
        let expected_intersections: BTreeSet<Point> = FromIterator::from_iter(vec![
            Point { x: 4, y: 0 },
            Point { x: 6, y: 5 },
            Point { x: 6, y: 3 },
        ]);
        let found_intersections = find_intersections(&horiz_segments, &vert_segments);
        assert!(
            expected_intersections == found_intersections,
            "Expected intersections: {:#?}\n Found intersections {:#?}",
            expected_intersections,
            found_intersections
        );
    }
}

