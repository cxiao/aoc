use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::vec::Vec;

fn parse(input: &str) -> Vec<u32> {
    input
        .trim()
        .split(',')
        .map(|n| {
            n.parse::<u32>()
                .expect(&format!("Could not parse value {}", n))
        })
        .collect()
}

fn compute(data: &mut Vec<u32>) {
    for i in (0..data.len()).step_by(4) {
        // Ensure there's enough instructions left
        if data.len() - i >= 4 {
            let opcode: u32 = data[i as usize];
            let x_idx: usize = data[i as usize + 1] as usize;
            let y_idx: usize = data[i as usize + 2] as usize;
            let result_idx: usize = data[i as usize + 3] as usize;
            match opcode {
                1 => { data[result_idx] = data[x_idx] + data[y_idx]; },
                2 => { data[result_idx] = data[x_idx] * data[y_idx]; },
                99 => break,
                _ => panic!("Error: Encountered unknown instruction {} at index {} of input data", opcode, i),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::compute;

    #[test]
    fn test_add() {
        let input = vec![1,0,0,0,99];
        let mut output = input.clone();
        let output_good = vec![2,0,0,0,99];
        compute(&mut output);
        assert_eq!(output_good, output);
    }

    #[test]
    fn test_mult_1() {
        let input = vec![2,3,0,3,99];
        let mut output = input.clone();
        let output_good = vec![2,3,0,6,99];
        compute(&mut output);
        assert_eq!(output_good, output);
    }

    #[test]
    fn test_mult_2() {
        let input = vec![2,4,4,5,99,0];
        let mut output = input.clone();
        let output_good = vec![2,4,4,5,99,9801];
        compute(&mut output);
        assert_eq!(output_good, output);
    }

    #[test]
    fn test_all_ops() {
        let input = vec![1,1,1,4,99,5,6,0,99];
        let mut output = input.clone();
        let output_good = vec![30,1,1,4,2,5,6,0,99];
        compute(&mut output);
        assert_eq!(output_good, output);
    }

}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
        if let Ok(mut infile) = File::open(&args[1]) {
            let mut data = String::new();
            infile.read_to_string(&mut data).unwrap();
            let input_data = parse(&data);
            let mut output_data = input_data.clone();
            compute(&mut output_data);
            println!("{:#?}", output_data);
        } else {
            println!("Invalid input filename. Exiting");
        }
    } else {
        println!("Incorrect number of arguments provided. Usage: aoc-2019-2-1 [INPUT_FILE]. Exiting");
    }
}
