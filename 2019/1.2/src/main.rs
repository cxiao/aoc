use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn calc_fuel(mass: i32) -> i32 {
    let fuel: i32 = (mass as f32 / 3f32).floor() as i32 - 2;
    if fuel > 0 {
        fuel + calc_fuel(fuel)
    } else {
        0
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
        if let Ok(infile) = File::open(&args[1]) {
            let infile_reader = BufReader::new(infile);
            let sum: i32 = infile_reader
                .lines()
                .map(|n| n.unwrap().parse::<i32>().unwrap())
                .map(|n| calc_fuel(n))
                .sum();
            println!("{}", sum);
        } else {
            println!("Error: Invalid filename. Exiting");
        }
    } else {
        println!("Error: Input argument not provided. Exiting");
    }
}
